<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function about()
    {
    	// return "about!";
    	return view('pages.about');
    }

    public function services()
    {
    	// return "services";
    	return view('pages.services');
    }

    public function compliance()
    {
    	// return "compliance";
    	return view('pages.compliance');
    }
}
