@extends('master')

@section('content')


    <div class="content">
      <div class="sub-banner">
          <div class="banner-shape"></div>
          <div class="container">
              <div class="banner-text-about">
                  <h2>About Us</h2>
                  <p>People helping people is our core philosophy at Accomplus.<br>Spanning advocacy, member services, regulatory and<br>governmental affairs, educational development and regional<br>management, our expertise is here to free up credit unions'<br>operating environment.</p>
              </div>
          </div>
      </div>

      <div class="sub-header-content">
        <div class="container">
          <div class="company-details">
            <p>Accomplus is part of the Illinois Credit Union League. It was formed to better help credit unions in Illinois and beyond stay competitive and efficient in the constantly evolving compliance environment.</p>
          </div>
        </div>
      </div>

      <div class="employees">
        <div class="container">
          <div class="employee">
          <div class="employee-image">

          </div>
          <div class="employee-text">
            <div class="inner-container">
              <div class="employee-header">
               
                  <h1>Stephen R. Olson</h1>
                  <h3>EVP & General Counsel</h3>
              
              </div>
              <div class="employee-info">
                
                  <p>Steve serves as executive vice president, general counsel and chief operating officer of Accomplus and the Illinois Credit Union League. In this role, he coordinates all dues-supported activities and directs all corporate legal affairs for the affiliated companies comprising the ICU System...</p>
            
              </div>
              <div class="employee-action">
                <a href="#">Read More</a>
                <a href="#">Contact</a>
              </div>
              </div>
            </div>
          </div>

          <div class="employee">
          <div class="employee-image">

          </div>
          <div class="employee-text">
            <div class="inner-container">
              <div class="employee-header">
               
                  <h1>Stephen R. Olson</h1>
                  <h3>EVP & General Counsel</h3>
              
              </div>
              <div class="employee-info">
                
                  <p>Steve serves as executive vice president, general counsel and chief operating officer of Accomplus and the Illinois Credit Union League. In this role, he coordinates all dues-supported activities and directs all corporate legal affairs for the affiliated companies comprising the ICU System...</p>
            
              </div>
              <div class="employee-action">
                <a href="#">Read More</a>
                <a href="#">Contact</a>
              </div>
              </div>
            </div>
          </div>

          <div class="employee">
          <div class="employee-image">

          </div>
          <div class="employee-text">
            <div class="inner-container">
              <div class="employee-header">
               
                  <h1>Stephen R. Olson</h1>
                  <h3>EVP & General Counsel</h3>
              
              </div>
              <div class="employee-info">
                
                  <p>Steve serves as executive vice president, general counsel and chief operating officer of Accomplus and the Illinois Credit Union League. In this role, he coordinates all dues-supported activities and directs all corporate legal affairs for the affiliated companies comprising the ICU System...</p>
            
              </div>
              <div class="employee-action">
                <a href="#">Read More</a>
                <a href="#">Contact</a>
              </div>
              </div>
            </div>
          </div>

          <div class="employee">
          <div class="employee-image">

          </div>
          <div class="employee-text">
            <div class="inner-container">
              <div class="employee-header">
               
                  <h1>Stephen R. Olson</h1>
                  <h3>EVP & General Counsel</h3>
              
              </div>
              <div class="employee-info">
                
                  <p>Steve serves as executive vice president, general counsel and chief operating officer of Accomplus and the Illinois Credit Union League. In this role, he coordinates all dues-supported activities and directs all corporate legal affairs for the affiliated companies comprising the ICU System...</p>
            
              </div>
              <div class="employee-action">
                <a href="#">Read More</a>
                <a href="#">Contact</a>
              </div>
              </div>
            </div>
          </div>

          <div class="employee">
          <div class="employee-image">

          </div>
          <div class="employee-text">
            <div class="inner-container">
              <div class="employee-header">
               
                  <h1>Stephen R. Olson</h1>
                  <h3>EVP & General Counsel</h3>
              
              </div>
              <div class="employee-info">
                
                  <p>Steve serves as executive vice president, general counsel and chief operating officer of Accomplus and the Illinois Credit Union League. In this role, he coordinates all dues-supported activities and directs all corporate legal affairs for the affiliated companies comprising the ICU System...</p>
            
              </div>
              <div class="employee-action">
                <a href="#">Read More</a>
                <a href="#">Contact</a>
              </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>


@stop