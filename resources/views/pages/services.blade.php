@extends('master')

@section('content')

    <div class="content">
      <div class="sub-banner">
          <div class="container">
            <div class="banner-shape"></div>
              <div class="banner-text-about alternative">
                <div class="contain-text">
                  <h2>Mission Accomplused</h2>
                  <p>Can you feel it? A more efficient operating environment is nearby. Look around and you’ll find that we have what you need to get there.</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="sub-header-content">
        <div class="container">
          <div class="company-details">
            <h3>Simplified Info. Collaborative Training.</h3>
            <p>Accomplus offers a suite of services across six specific categories. Whether it’s a review of your policies and procedures, a plan to guide you toward the future or essential training and consulting, one thing’s for sure: All of them help your credit union succeed.</p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="boxes">
            <div class="box">
                <div class="box-title"><h4>Compliance Reviews</h4></div>
                  <div class="box-content">
                  <div class="inner-container box-text">
                    <p>Ensure that your policies and procedures are where they should be.</p>
                    </div>
                  </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
                <div class="box-title"><h4>Planning</h4></div>
                <div class="box-content">
                  <div class="inner-container box-text">
                    <p>We’ll work with you to define your current and future objectives.</p>
                    </div>
                  </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>Regulatory Consulting</h4></div>
               <div class="box-content">
                 <div class="inner-container box-text">
                   <p>Need help with your next big thing? We’ll work with you directly.</p>
                   </div>
                 </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>Training</h4></div>
               <div class="box-content">
                <div class="inner-container box-text">
                  <p>Take care of your annual requirements here.</p>
                  </div>
                </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>Employment</h4></div>
                <div class="box-content">
                  <div class="inner-container box-text">
                  <p>Are your bases covered? Find out here.</p>
                  </div>
                </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>

            <div class="box">
               <div class="box-title"><h4>Back Office Assistance</h4></div>
                <div class="box-content">
                  <div class="inner-container box-text">
                  <p>You don’t have to do it all. Accomplus can help.</p>
                  </div>
                </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
          </div>
      </div>

@stop

