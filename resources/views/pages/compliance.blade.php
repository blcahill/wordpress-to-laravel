@extends('master')

@section('content')

    <div class="content">
      <div class="sub-banner alternative">
          <div class="container">
            <div class="banner-shape"></div>
              <div class="banner-text-about alternative">
                <div class="contain-text">
                  <h2>Don't just compete. Prosper.</h2>
                  <p>These days, regulations change fast. We don't just help you keep up—we help you concentrate on the growth of your CU.</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="sub-header-content">
        <div class="container">
          <div class="company-details">
            <h3>Explore. Implement. Grow.</h3>
            <p>Accomplus equips you with the information you need to better understand rules and regulations for operating your credit union.</p>
            <p>As you explore the categories below, remember that you can <a href="#">contact us anytime</a> if you cant find what youre looking for.</p>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="boxes">
            <div class="box">
                <div class="box-title"><h4>member accounts</h4></div>
                  <div class="box-content">
                  <div class="inner-container box-text">
                    <p>Deposit, Fiduciary and Electronic Payments</p>
                    </div>
                  </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
                <div class="box-title"><h4>lending</h4></div>
                <div class="box-content">
                  <div class="inner-container box-text">
                    <p>Loans, Leasing, Credit Cards and Real Estate</p>
                    </div>
                  </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>operations</h4></div>
               <div class="box-content">
                 <div class="inner-container box-text">
                   <p>Advertising, BSA, Employment, and Legal</p>
                   </div>
                 </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>governance</h4></div>
               <div class="box-content">
                <div class="inner-container box-text">
                  <p>Board, Continuity Planning, Field of Membership and more</p>
                  </div>
                </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
            <div class="box">
               <div class="box-title"><h4>it</h4></div>
                <div class="box-content">
                  <div class="inner-container box-text">
                  <p>Social media security and seven branches of cybersecurity</p>
                  </div>
                </div>
                <div class="box-link"><h5><a href="#">view all</a></h5></div>
            </div>
          </div>
      </div>

@stop