@extends('master')

@section('content')


    	  <div class="content">

    	    <div class="slider-container ">
    	        <div class="slider">
    	              <div id="one" class="slide">
    	                <div class="container">
    	                  <div class="banner-text-main">
    	                    <h1>Welcome to efficiency.</h1>
    	                    <p>Accomplus is a resource for you to use whenever you need it. Easily keep up with the ever-changing regulatory landscape.</p>
    	                    <a href="#">Log In</a>
    	                  </div>
    	                </div>
    	              </div>

    	       <!--        <div id="two" class="slide">
    	                <div class="container">
    	                  <div class="banner-text">
    	                    <h1>We're here to serve.</h1>
    	                    <p>After using Accomplus’ services, your CU will be a well-oiled machine.</p>
    	                    <a href="#">Explore Services</a>
    	                  </div>
    	                </div>
    	              </div> -->
<!-- 
    	              <div id="three" class="slide">
    	                <div class="container">
    	                  <div class="banner-text">
    	                    <h1>The compliance hub</h1>
    	                    <p>Explore our vast knowledge base (and use the search tool) to quickly find what you're looking for.</p>
    	                    <a href="#">Learn More</a>
    	                  </div>
    	                </div>
    	              </div> -->

    	         <!--      <div id="four" class="slide">
    	                <div class="container">
    	                  <div class="banner-text">
    	                  <h1>We want to hear from you!
    	                  </h1>
    	                  <p>Is there a way your Accomplus experience could be even better?</p>
    	                  <a href="#">Get In Touch</a>
    	                </div>
    	                </div>
    	              </div> -->

    	        </div>
    	        <div class="arrows">
    	          <div class="container">
    	          <div class="left-arrow">
    	            <a href=""><i class="fa fa-arrow-left"></i></a>
    	          </div>

    	          <div class="right-arrow">
    	            <a href=""><i class="fa fa-arrow-right"></i></a>
    	          </div>
    	          </div>
    	        </div>

    	    </div>

    	      <div class="top-content">
    	        <div class="container">
    	          <div class="top-text">
    	            <div class="inner-container">
    	            <h1>Our mission</h1>
    	            <p>At Accomplus, our priority is to be a one-stop compliance shop for credit unions. Our industry is always changing, and it can be hard to keep up. That’s where we come in. If you need compliance help, you’ll get a collaborative and simplified experience here. With sophisticated support and best-in-class services, we strive to help your CU operate more efficiently than ever before.</p>
    	            </div>
    	            </div>

    	            <div class="top-slider">
    	              <div class="inner-container">
    	                <div class="slides">
    	                  <h4>Consulting</h4>
    	                  <div class="grey-line">
    	                  </div>
    	                  <p>Need help with a Field of Membership expansion?</p>
    	                  <a href="#" class="slider-link">Learn More</a>
    	                  <div class="green-line">
    	                  </div>
    	                </div>
    	                <div class="radio-buttons">
    	                    <div class="slide-button">
    	                      <div class="slide-radio-button checked">
    	                      </div>
    	                    </div>
    	                    <div class="slide-button">
    	                      <div class="slide-radio-button">
    	                    </div>
    	                    </div>
    	                    <div class="slide-button">
    	                      <div class="slide-radio-button ">
    	                    </div>
    	                    </div>
    	                  </div>
    	                  </form>
    	                </div>
    	            </div>
    	            </div>
    	        </div>
    	      

    	      <div class="bottom-content">
    	        <div class="container">
    	          <div class="right-action">
    	            <div class="inner-container">
    	            <div class="icon">
    	              <h1 class="bracket">[</h1><i class="fa fa-user fa-2x"></i><h1 class="bracket">]</h1>
    	            </div>
    	            <div class="action-header">
    	              <h2>Our Staff</h2>
    	            </div>
    	            <div class="action-content">
    	              <p>Experienced, driven and ready to help however we can.</p>
    	            </div>
    	            <div class="action-link">
    	              <a href="#">About Us</a>
    	            </div>
    	          </div>
    	          </div>
    	          <div class="middle-action">
    	            <div class="inner-container">
    	            <div class="icon">
    	              <h1 class="bracket">[</h1><i class="fa fa-check fa-2x"></i><h1 class="bracket">]</h1>
    	            </div>
    	            <div class="action-header">
    	              <h2>Our Services</h2>
    	            </div>
    	            <div class="action-content">
    	              <p>A better operating environment is around the corner.</p>
    	            </div>
    	            <div class="action-link">
    	              <a href="#">Learn More</a>
    	            </div>
    	          </div>
    	          </div>

    	          <div class="left-action">
    	            <div class="top-action">
    	              <div class="inner-container">
    	                <div class="icon action-header">
    	                  <h2>Questions?</h2>
    	                </div>
    	                <div class="action-link">
    	                  <a href="#">Contact Us</a>
    	    
    	                </div>
    	              </div>
    	            </div>

    	            <div class="bottom-action">
    	              <div class="container">
    	                <div class="icon action-header">
    	                  <h2>Calendar</h2>
    	                </div>
    	                <div class="event">
    	                  <p>Security update</p>
    	                </div>
    	                <div class="event">
    	                  <p>IRS Form 5498</p>
    	                </div>
    	                <div class="action-link">
    	                  <a href="#">See All Events</a>
    	                </div>
    	              </div>
    	            </div>
    	          </div>
    	        </div>
    	      </div>
@stop