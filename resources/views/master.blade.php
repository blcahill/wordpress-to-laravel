<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Accomplus</title>
  <link rel="stylesheet" href="{{asset('css/compliance.css')}}">
  <link rel="stylesheet" href="{{asset('css/services.css')}}">
  <link rel="stylesheet" href="{{asset('css/about.css')}}">
  <link rel="stylesheet" href="{{asset('css/header.css')}}">
  <link rel="stylesheet" href="{{asset('css/footer.css')}}">
  <link rel="stylesheet" href="{{asset('css/home.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
  <div class="page-container">
    <header>
      <div class="contact-details">
        <div class="container">
          <ul>
          <li><a href="#" class="action">Login</a></li>
          <li><a href="#" class="action">Contact Us</a></li>
          <li><p class="phone">888-415-6145</p></li>
          </ul>
        </div>
    </div>
    <div class="white-space">
      <div class="container">
        <div class="accomplus-logo-header">
          <a href="{{ URL::to('/') }}" class="home-link"></a>
        </div>
      </div>
    </div>
    <div class="navbar">
      <div class="container">
        <ul>
          <li class="nav-list-item">
            <a href="{{ URL::to('compliance') }}" class="nav-item">Compliance</a></li>
          <li class="nav-list-item"><a href="{{ URL::to('services') }}" class="nav-item">Serivces</a></li>
          <li class="nav-list-item"><a href="#" class="nav-item">Resources</a></li>
          <li class="nav-list-item"><a href="{{ URL::to('about') }}" class="nav-item">About Us</a></li>
        </ul>

        <div class="search">
          <form>
            <input type="search" class="search-field" name="firstname">
            <i class="fa fa-search"></i>
          </form>
        </div>

      </div>
      </div>
    </header>

    @yield('content')

        </div>
        <footer>
        <div class="footer-logo">
        </div>
        <div class="container">
        <div class="address">
          <div class="location-pin">
            <i class="fa fa-map-marker"></i>
          </div>

          <div class="address-container">
            <p class="company">Accomplus</p>
            <p>1807 West Diehl Road</p>
            <p>P.O. Box 3107</p>
            <p>Naperville, IL 60566-7107</p>
            <p>Phone: 888-415-6145</p>
            <p>Email: info@accomplus.net | 
      About Us Privacy Policy ©2015 ICUL Accomplus</p>
          </div>
          </div>
        </div>
        </footer>
      </div>
    </body>
    </html>